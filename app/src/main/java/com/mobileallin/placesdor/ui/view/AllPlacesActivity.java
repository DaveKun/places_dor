package com.mobileallin.placesdor.ui.view;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.mobileallin.placesdor.R;
import com.mobileallin.placesdor.data.database.PlaceContract.PlaceEntry;
import com.mobileallin.placesdor.ui.adapter.PlaceCursorAdapter;
import com.mobileallin.placesdor.ui.utils.DialogUtils;
import com.mobileallin.placesdor.ui.utils.LocationUtils;
import com.mobileallin.placesdor.ui.utils.SharedPrefUtils;


public class AllPlacesActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final int PLACE_LOADER = 0;

    private PlaceCursorAdapter mCursorAdapter;
    private View emptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_places_activity);

        LocationUtils locationMethods = new LocationUtils();
        SharedPrefUtils sharedPreferencesMethods = new SharedPrefUtils();
        DialogUtils dialogMethods = new DialogUtils();
        mCursorAdapter = new PlaceCursorAdapter(this, null);
        emptyView = findViewById(R.id.empty_view);

        RecyclerView mRecyclerView = findViewById(R.id.list_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AllPlacesActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mCursorAdapter);

        if (!sharedPreferencesMethods.getFromSP("dontshow", AllPlacesActivity.this)) {
            dialogMethods.showIntroDialog(AllPlacesActivity.this);
        }

        FloatingActionButton fab = findViewById(R.id.all_places_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllPlacesActivity.this, EditPlaceActivity.class);
                startActivity(intent);
            }
        });

        ItemTouchHelper.SimpleCallback _ithCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                /*get the viewHolder's and target's positions in your adapter data, swap them*/
                mCursorAdapter.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            }

            /*defines the enabled move directions in each state (idle, swiping, dragging).*/
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
                        ItemTouchHelper.DOWN | ItemTouchHelper.UP | ItemTouchHelper.START | ItemTouchHelper.END);
            }
        };

        ItemTouchHelper ith = new ItemTouchHelper(_ithCallback);
        ith.attachToRecyclerView(mRecyclerView);

        getLoaderManager().initLoader(PLACE_LOADER, null, this);

        if (!locationMethods.checkPermission(getApplicationContext())) {
            locationMethods.showPermissionDialog(AllPlacesActivity.this);
        }

        locationMethods.showEnableLocationDialog(AllPlacesActivity.this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = {
                PlaceEntry._ID,
                PlaceEntry.COLUMN_PLACE_NAME,
                PlaceEntry.COLUMN_PLACE_LATITUDE,
                PlaceEntry.COLUMN_PLACE_LONGITUDE,
                PlaceEntry.COLUMN_PLACE_PICTURE};

        return new CursorLoader(this,   // Parent activity context
                PlaceEntry.CONTENT_URI,   // Provider content URI to query
                projection,             // Columns to include in the resulting Cursor
                null,                   // No selection clause
                null,                   // No selection arguments
                null);                  // Default sort order
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (!cursor.moveToFirst()) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
        }
        mCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);
    }

    public void onItemClick(long id) {
        Intent intent = new Intent(AllPlacesActivity.this, EditPlaceActivity.class);

        Uri currentPlaceUri = ContentUris.withAppendedId(PlaceEntry.CONTENT_URI, id);
        intent.setData(currentPlaceUri);

        startActivity(intent);
    }
}
