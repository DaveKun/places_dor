package com.mobileallin.placesdor.ui.adapter;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobileallin.placesdor.R;
import com.mobileallin.placesdor.data.database.PlaceContract.PlaceEntry;
import com.mobileallin.placesdor.ui.utils.PictureUtils;
import com.mobileallin.placesdor.ui.view.AllPlacesActivity;


/**
 * Created by Dawid on 2017-08-16.
 */

public class PlaceCursorAdapter extends CursorRecyclerAdapter<PlaceCursorAdapter.ViewHolder> {

    private AllPlacesActivity placesActivity = new AllPlacesActivity();
    private PictureUtils pictureMethods = new PictureUtils();

    public PlaceCursorAdapter(AllPlacesActivity context, Cursor c) {
        super(c);
        this.placesActivity = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected TextView nameTextView;
        protected TextView latitudeTextView;
        protected TextView longitudeTextView;
        protected ImageView placeImage;

        public ViewHolder(View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.name);
            latitudeTextView = itemView.findViewById(R.id.place_latitude);
            longitudeTextView = itemView.findViewById(R.id.place_longitude);
            placeImage = itemView.findViewById(R.id.place_main_img);
        }
    }

    @Override
    public PlaceCursorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;
    }


    @Override
    public Cursor getCursor() {
        return super.getCursor();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        final long id;

        /*Find the columns of place attributes that we're interested in*/
        id = cursor.getLong(cursor.getColumnIndex(PlaceEntry._ID));
        int nameColumnIndex = cursor.getColumnIndex(PlaceEntry.COLUMN_PLACE_NAME);
        int latitudeColumnIndex = cursor.getColumnIndex(PlaceEntry.COLUMN_PLACE_LATITUDE);
        int longitudeColumnIndex = cursor.getColumnIndex(PlaceEntry.COLUMN_PLACE_LONGITUDE);
        int imageColumnIndex = cursor.getColumnIndex(PlaceEntry.COLUMN_PLACE_PICTURE);

        /*Read the place attributes from the Cursor for the current place*/
        String placeName = cursor.getString(nameColumnIndex);
        String placeLatitude = String.valueOf(cursor.getDouble(latitudeColumnIndex));
        String placeLongitude = String.valueOf(cursor.getDouble(longitudeColumnIndex));

        byte[] listplaceImage = cursor.getBlob(imageColumnIndex);

        if (listplaceImage != null) {
            Bitmap imageItemBitmapResource = pictureMethods.convertByteArrToBitmap(listplaceImage);
            viewHolder.placeImage.setImageBitmap(imageItemBitmapResource);
        } else {
            viewHolder.placeImage.setImageResource(R.drawable.ic_add_picture);
        }

        viewHolder.nameTextView.setText(placeName);
        viewHolder.latitudeTextView.setText(placeLatitude);
        viewHolder.longitudeTextView.setText(placeLongitude);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                placesActivity.onItemClick(id);
            }
        });
    }
}
