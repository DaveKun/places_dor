package com.mobileallin.placesdor.ui.utils;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.mobileallin.placesdor.R;
import com.mobileallin.placesdor.data.database.PlaceContract;
import com.mobileallin.placesdor.ui.view.AllPlacesActivity;
import com.mobileallin.placesdor.ui.view.EditPlaceActivity;

/**
 * Created by Dawid on 2017-08-27.
 */

public class PlaceUtils {

    private DialogUtils dialogMethods;
    private AllPlacesActivity allPlacesActivity;

    public void deletePlace(Uri mCurrentPlaceUri, Context context) {
        /*Only perform the delete if this is an existing place.*/
        if (mCurrentPlaceUri != null) {
            int rowsDeleted = context.getContentResolver().delete(mCurrentPlaceUri, null, null);
            if (rowsDeleted == 0) {
                Toast.makeText(context, context.getString(R.string.editor_delete_place_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, context.getString(R.string.editor_delete_place_successful),
                        Toast.LENGTH_SHORT).show();
            }
        }
        /*Close the activity*/
        ((Activity) context).finish();
    }

    public void savePlace(Context context, EditText mNameEditText, Uri mCurrentPlaceUri,
                          boolean pictureTaken, boolean mEditPictureChanged, byte[] mPicture, byte[] placePictureBytes,
                          double latitudeDouble, double longitudeDouble) {
        dialogMethods = new DialogUtils();
        allPlacesActivity = new AllPlacesActivity();

        String nameString = mNameEditText.getText().toString().trim();
        if (mCurrentPlaceUri != null) {
            pictureTaken = true;
        }
        if (TextUtils.isEmpty(nameString)
                || !pictureTaken) {
            dialogMethods.showMustDeclareAllValuesDialog(context);
            EditPlaceActivity.placeCanBeSaved = false;
            return;
        } else {
            EditPlaceActivity.placeCanBeSaved = true;
        }

        /*User only edited the name not the photo, photo stays the same as in Db*/
        if (mCurrentPlaceUri != null && !mEditPictureChanged) {
            mPicture = placePictureBytes;
        }

        ContentValues values = new ContentValues();
        values.put(PlaceContract.PlaceEntry.COLUMN_PLACE_NAME, nameString);
        values.put(PlaceContract.PlaceEntry.COLUMN_PLACE_LATITUDE, latitudeDouble);
        values.put(PlaceContract.PlaceEntry.COLUMN_PLACE_LONGITUDE, longitudeDouble);
        values.put(PlaceContract.PlaceEntry.COLUMN_PLACE_PICTURE, mPicture);

        /*Determine if this is a new or existing place by checking if mCurrentPlaceUri is null or not*/
        if (mCurrentPlaceUri == null) {
            /*This is a NEW place, so insert a new place into the provider*/
            Uri newUri = context.getContentResolver().insert(PlaceContract.PlaceEntry.CONTENT_URI, values);
            if (newUri == null) {
                Toast.makeText(context, context.getString(R.string.editor_insert_place_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, context.getString(R.string.editor_insert_place_successful),
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            pictureTaken = true;
            int rowsAffected = context.getContentResolver().update(mCurrentPlaceUri, values, null, null);
            if (rowsAffected == 0) {
                Toast.makeText(context, context.getString(R.string.editor_update_place_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, context.getString(R.string.editor_update_place_successful),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}
