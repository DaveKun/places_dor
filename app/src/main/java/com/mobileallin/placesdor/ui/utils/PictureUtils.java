package com.mobileallin.placesdor.ui.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;

import java.io.ByteArrayOutputStream;

import static com.mobileallin.placesdor.ui.view.EditPlaceActivity.REQUEST_IMAGE_CAPTURE;

/**
 * Created by Dawid on 2017-08-21.
 */

public class PictureUtils {

    public void takePictureOfThePlace(Context context) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            ((Activity) context).startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    public Bitmap convertByteArrToBitmap(byte[] byteArr) {
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArr, 0, byteArr.length);
        return bitmap;
    }

    public byte[] convertImageBitmapToByteArr(Bitmap imageBitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }
}
