package com.mobileallin.placesdor.ui.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.view.Gravity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobileallin.placesdor.R;

/**
 * Created by Dawid on 2017-08-27.
 */

public class DialogUtils {

    private SharedPrefUtils sharedPreferencesMethods;

    public void showMustDeclareAllValuesDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.empty_values_msg);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void showUnsavedChangesDialog(
            DialogInterface.OnClickListener discardButtonClickListener, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.unsaved_changes_dialog_msg);
        builder.setPositiveButton(R.string.discard, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void showDeleteConfirmationDialog(final Uri mCurrentPlaceUri, Context context) {
        final PlaceUtils placeMethods = new PlaceUtils();
        final Context fContext = context;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.delete_dialog_msg);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                placeMethods.deletePlace(mCurrentPlaceUri, fContext);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void showIntroDialog(Context context) {
        final Context fContext = context;
        sharedPreferencesMethods = new SharedPrefUtils();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        ImageView image = new ImageView(context);
        image.setImageResource(R.mipmap.ic_launcher);
        builder.setIcon(R.mipmap.ic_launcher_round)
                .setTitle("Changing place position")
                .setView(image)
                .setMessage("You can change each place position by holding it for a while and " +
                        "using drag and drop to move it to the desired place on the list")
                .setNegativeButton("GOT IT!", null)
                .setPositiveButton("Don't Show Me Again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sharedPreferencesMethods.saveInSP("dontshow", true, fContext);
                    }
                });
        builder.setCancelable(false);
        AlertDialog about = builder.create();
        about.show();
        TextView messageText = about.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);
        Button nbutton = about.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(Color.BLACK);
    }
}
