package com.mobileallin.placesdor.ui.view;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.mobileallin.placesdor.R;
import com.mobileallin.placesdor.data.database.PlaceContract.PlaceEntry;
import com.mobileallin.placesdor.ui.utils.DialogUtils;
import com.mobileallin.placesdor.ui.utils.LocationUtils;
import com.mobileallin.placesdor.ui.utils.PictureUtils;
import com.mobileallin.placesdor.ui.utils.PlaceUtils;


/**
 * Created by Dawid on 2017-08-16.
 */

public class EditPlaceActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor>, LocationListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int EXISTING_PLACE_LOADER = 0;
    public static final int TAG_CODE_PERMISSION_LOCATION = 5432;
    public static final int REQUEST_IMAGE_CAPTURE = 2468;

    private Uri mCurrentPlaceUri;
    private ImageView mPictureImageView;
    private EditText mNameEditText;

    private PictureUtils pictureMethods;
    private PlaceUtils placeMethods;
    private DialogUtils dialogMethods;

    private byte[] mPicture;
    private boolean pictureTaken = false;
    private double latitudeDouble;
    private double longitudeDouble;
    private boolean mPlaceHasChanged = false;
    private boolean mEditPictureChanged = false;

    private LatLng mLatLng;
    private MapView mMapView;

    private boolean isNewPlace;
    private String placeName;
    byte[] placePictureBytes;

    public static boolean placeCanBeSaved = true;

    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            mPlaceHasChanged = true;
            return false;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_place_activity);

        Intent intent = getIntent();
        mCurrentPlaceUri = intent.getData();
        LocationUtils locationMethods = new LocationUtils();
        pictureMethods = new PictureUtils();
        placeMethods = new PlaceUtils();
        dialogMethods = new DialogUtils();

        mMapView = findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);

        mPictureImageView = findViewById(R.id.editor_picture_image_view);
        mNameEditText = findViewById(R.id.edit_place_name);

        TextView tapImageTextView = findViewById(R.id.tap_image_text_view);

        locationMethods.showEnableLocationDialog(EditPlaceActivity.this);

        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());

         /*If the intent DOES NOT contain a place content URI, then we know that we are
        creating a new place.*/
        if (mCurrentPlaceUri == null) {
            isNewPlace = true;
            setTitle(getString(R.string.editor_activity_title_new_place));
            invalidateOptionsMenu();
        } else {
            isNewPlace = false;
            setTitle(getString(R.string.editor_activity_title_edit_place));
            tapImageTextView.setVisibility(View.VISIBLE);

            mPictureImageView.setVisibility(View.VISIBLE);
            getLoaderManager().initLoader(EXISTING_PLACE_LOADER, null, this);
        }

        mNameEditText.setOnTouchListener(mTouchListener);
        mPictureImageView.setOnTouchListener(mTouchListener);

        mPictureImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pictureMethods.takePictureOfThePlace(EditPlaceActivity.this);
                mEditPictureChanged = true;
            }
        });

        /*Location related*/
        if (locationMethods.checkPermission(getApplicationContext())) {
        /*Force Location updates*/
            locationMethods.requestLocationUpdates(EditPlaceActivity.this);

            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                        /* Got last known location. In some rare situations this can be null.*/
                            if (location != null) {
                                Toast.makeText(getApplicationContext(), "Your location is:" + location.getLatitude() + " " + location.getLongitude(), Toast.LENGTH_SHORT).show();
                                latitudeDouble = location.getLatitude();
                                longitudeDouble = location.getLongitude();

                                setUpGoogleMap(location);
                            }
                        }
                    });
        } else {
            locationMethods.showPermissionDialog(EditPlaceActivity.this);
        }
    }

    private void setUpGoogleMap(Location location) {

        final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                if (!isNewPlace) {
                    googleMap.addMarker(new MarkerOptions()
                            .position(mLatLng)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                            .title(placeName));
                }
                googleMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                        .snippet("This location will be saved")
                        .title("You are here"));

                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));
            }
        });
    }

    @Override
    public android.content.Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = {
                PlaceEntry._ID,
                PlaceEntry.COLUMN_PLACE_NAME,
                PlaceEntry.COLUMN_PLACE_LATITUDE,
                PlaceEntry.COLUMN_PLACE_LONGITUDE,
                PlaceEntry.COLUMN_PLACE_PICTURE};

        /* This loader will execute the ContentProvider's query method on a background thread*/
        return new CursorLoader(this,   // Parent activity context
                mCurrentPlaceUri,         // Query the content URI for the current place
                projection,             // Columns to include in the resulting Cursor
                null,                   // No selection clause
                null,                   // No selection arguments
                null);                  // Default sort order
    }

    @Override
    public void onLoadFinished(android.content.Loader<Cursor> loader, Cursor cursor) {
        /*Bail early if the cursor is null or there is less than 1 row in the cursor*/
        if (cursor == null || cursor.getCount() < 1) {
            return;
        }

        if (cursor.moveToFirst()) {
            int nameColumnIndex = cursor.getColumnIndex(PlaceEntry.COLUMN_PLACE_NAME);
            int latitudeColumnIndex = cursor.getColumnIndex(PlaceEntry.COLUMN_PLACE_LATITUDE);
            int longitudeColumnIndex = cursor.getColumnIndex(PlaceEntry.COLUMN_PLACE_LONGITUDE);
            int pictureColumnIndex = cursor.getColumnIndex(PlaceEntry.COLUMN_PLACE_PICTURE);

            placeName = cursor.getString(nameColumnIndex);
            String latitude = cursor.getString(latitudeColumnIndex);
            String longitude = cursor.getString(longitudeColumnIndex);

            double latitudeDouble = Double.parseDouble(latitude);
            double longitudeDouble = Double.parseDouble(longitude);

            mLatLng = new LatLng(latitudeDouble, longitudeDouble);
            placePictureBytes = cursor.getBlob(pictureColumnIndex);

            if (placePictureBytes != null) {
                Bitmap pictureBitmap = pictureMethods.convertByteArrToBitmap(placePictureBytes);
                mPictureImageView.setImageBitmap(pictureBitmap);
            }
            mNameEditText.setText(placeName);
        }
    }

    @Override
    public void onLoaderReset(android.content.Loader<Cursor> loader) {
        mNameEditText.setText("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_save:
                placeMethods.savePlace(EditPlaceActivity.this, mNameEditText, mCurrentPlaceUri,
                        pictureTaken, mEditPictureChanged, mPicture,
                        placePictureBytes, latitudeDouble, longitudeDouble);
                if (placeCanBeSaved) {
                    finish();
                }
                return true;

            case R.id.action_delete:
                dialogMethods.showDeleteConfirmationDialog(mCurrentPlaceUri, EditPlaceActivity.this);
                return true;

            case android.R.id.home:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        /*If this is a new place, hide the "Delete" menu item.*/
        if (mCurrentPlaceUri == null) {
            MenuItem menuItem = menu.findItem(R.id.action_delete);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            mPictureImageView.setVisibility(View.VISIBLE);
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            mPictureImageView.setImageBitmap(imageBitmap);
            mPicture = pictureMethods.convertImageBitmapToByteArr(imageBitmap);
            pictureTaken = true;
        }
    }

    @Override
    public void onBackPressed() {
        /*If the place hasn't changed, continue with handling back button press*/
        if (!mPlaceHasChanged) {
            super.onBackPressed();
            return;
        }

        DialogInterface.OnClickListener discardButtonClickListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                };
        dialogMethods.showUnsavedChangesDialog(discardButtonClickListener, EditPlaceActivity.this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Toast.makeText(this, "onConnected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, "onConnectionSuspended", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "onConnectionFailed", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
    }

    /*Calling map methods here makes the Google map load faster*/
    @Override
    protected void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        mMapView.onLowMemory();
        super.onLowMemory();
    }
}
