package com.mobileallin.placesdor.ui.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Dawid on 2017-08-27.
 */

public class SharedPrefUtils {

    /*Save / retrieve from Shared Preferences*/
    public boolean getFromSP(String key, Context context) {
        SharedPreferences preferences = context.getSharedPreferences("PlacesDOR", android.content.Context.MODE_PRIVATE);
        return preferences.getBoolean(key, false);
    }

    public void saveInSP(String key, boolean value, Context context) {
        SharedPreferences preferences = context.getSharedPreferences("PlacesDOR", android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }
}
