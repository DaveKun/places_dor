package com.mobileallin.placesdor.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mobileallin.placesdor.data.database.PlaceContract.PlaceEntry;


/**
 * Created by Dawid on 2017-08-16.
 */

public class PlaceDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "places.db";

    /**
     * Database version. If you change the database schema, you must increment the database version.
     */
    private static final int DATABASE_VERSION = 1;

    public PlaceDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_PLACESS_TABLE = "CREATE TABLE " + PlaceEntry.TABLE_NAME + " ("
                + PlaceEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + PlaceEntry.COLUMN_PLACE_NAME + " TEXT NOT NULL, "
                + PlaceEntry.COLUMN_PLACE_LATITUDE + " DOUBLE NOT NULL, "
                + PlaceEntry.COLUMN_PLACE_LONGITUDE + " DOUBLE NOT NULL, "
                + PlaceEntry.COLUMN_PLACE_PICTURE + " BLOB);";

        db.execSQL(SQL_CREATE_PLACESS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /* The database is still at version 1, so there's nothing to do be done here.*/
    }
}