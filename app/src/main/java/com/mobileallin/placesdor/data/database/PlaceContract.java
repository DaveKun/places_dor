package com.mobileallin.placesdor.data.database;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Dawid on 2017-08-16.
 */

public class PlaceContract {

    private PlaceContract() {
    }

    public static final String CONTENT_AUTHORITY = "com.mobileallin.placesdor";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_PLACES = "places";

    public static final class PlaceEntry implements BaseColumns {

        /**
         * The content URI to access the place data in the provider
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_PLACES);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PLACES;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PLACES;

        public final static String TABLE_NAME = "places";

        public final static String _ID = BaseColumns._ID;

        public final static String COLUMN_PLACE_NAME = "name";

        public final static String COLUMN_PLACE_LATITUDE = "latitude";

        public final static String COLUMN_PLACE_LONGITUDE = "longitude";

        public final static String COLUMN_PLACE_PICTURE = "picture";
    }
}
