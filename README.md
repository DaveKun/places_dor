# README #

### What is this repository for? ###

The application saves user favourite places to a database. Each place consists of a name, Location and a picture.
Each place can be also edited or deleted. You can also change the place's position in the main view.

### Application functionalities ###

1. Checking the user Location
2. Saving the Location with a place name to the database
3. Performing CRUD database operations
4. Changing the places order on the list by dragging and dropping them to the desired position
5. Showing the Location on a Google map
6. Taking a picture of the place 
7. Editing the place in a separate view
8. Landscape view support (only the main activity)

### Chosen concepts and solutions ###

1. The database uses BaseColumns and SQLiteOpenHelper
2. The data is shown as a RecyclerView list.
3. The app listens for Location updates in 30m, or 1 minute. This can be changed in EditPaceActivity, requestLocationUpdates method. 

### How do I get set up? ###

Just open the app in Android Studio and you're good to go.

### Who do I talk to? ###

Dawid Kunicki
dawidkunicki88@gmail.com